import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueFullPage from 'vue-fullpage.js'
import router from './router.js';
import vSelect from 'vue-select'

import 'bootstrap-css-only/css/bootstrap.min.css'; 
import 'mdbvue/build/css/mdb.css';

Vue.config.productionTip = false
Vue.use(VueAxios, axios)
Vue.use(VueFullPage);

Vue.component('v-select', vSelect)

// axios.defaults.baseURL = '//arcane-woodland-98207.herokuapp.com/'

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
